typedef unsigned char byte;

byte statesLow[256];
byte statesHigh[256];

byte regData[8];

void setup()
{
	DDRA = 255;			// data
	DDRB |= B00000011;  // B0 - clock, B1 - latch
	DDRC = 255;			// layers

	for( byte i = 0; i < 9; i++ )
		statesLow[i] = 1;
	statesLow[0] = 255;
}

inline void regOut()
{
	PORTA = regData[7];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[6];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[5];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[4];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[3];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[2];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[1];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTA = regData[0];
	PORTB &= ~( 1 << B0 );
	PORTB |= 1 << B0;

	PORTB &= ~( 1 << B1 );
	PORTB |= 1 << B1;
}

void loop()
{
	byte timer = 0;
	byte data = 0;
	byte state = 0;

	while( true )
	{
		for( byte layersLow = 0; layersLow < 4; ++layersLow )
		{
			byte layerOffset = layersLow << 6;
			for( byte iByte = 0; iByte < 8; ++iByte )
			{
				for( byte iBit = 56; iBit != 248; iBit -= 8 )
				{
					data <<= 1;
					state = statesLow[iBit + iByte + layerOffset];
					data |= state >= timer && state;
				}
				regData[iByte] = data;
			}
			
			PORTC = 0;
			regOut();
			PORTC |= 1 << layersLow;

			//delay( 1000 );
		}

		for( byte layersHigh = 0; layersHigh < 4; ++layersHigh )
		{
			byte layerOffset = layersHigh << 6;
			for( byte iByte = 0; iByte < 8; ++iByte )
			{
				for( byte iBit = 56; iBit != 248; iBit -= 8 )
				{
					data <<= 1;
					state = statesHigh[iBit + iByte + layerOffset];
					data |= state >= timer && state;
				}
				regData[iByte] = data;
			}
			
			PORTC = 0;
			regOut();
			PORTC |= 1 << ( layersHigh + 4 );
		}

		//delay( 1000 );

		timer++;
	}
}