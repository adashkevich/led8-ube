#define F_CPU 16000000UL // ������� ������� �����������
#define Baudrate 115200 // �������� ������ �������
#define ClockBit 0
#define LatchBit 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

typedef unsigned char byte;

byte states512Low[256] = {};
byte states512High[256] = {};
byte states64[64] = {};
byte states8[8] = {};
byte usedLayers = 255;
byte s64Update = false;

byte regData[8] = {};

enum FrameType { None, PWMType, UsedLayers, Data512, Data64, Data8 };
enum PWM { OnColumns, OnLayers };

FrameType frameType = None;
PWM pwmType = OnColumns;
short int byteCounter = 0;

byte timerStep = 1;

inline void regOut()
{
	PORTA = regData[7];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[6];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[5];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[4];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[3];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[2];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[1];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTA = regData[0];
	PORTB &= ~( 1 << ClockBit );
	PORTB |= 1 << ClockBit;

	PORTB &= ~( 1 << LatchBit );
	PORTB |= 1 << LatchBit;
}

void UARTInit()
{
	// ��������� ����������: 8 ��� ������, 1 �������� ���, ��� �������� ��������
	// USART ��������: �������
	// USART ����������: �������
	// USART �����: �����������

	UBRR0L = ( F_CPU / Baudrate / 16 - 1 ); // ��������� �������� ������ �������
	UBRR0H = ( F_CPU / Baudrate / 16 - 1 ) >> 8;

	UCSR0B |= ( 1 << RXCIE0 ) | // ��������� ���������� �� ���������� ������ ������
		( 1 << RXEN0 ) | ( 1 << TXEN0 ); // �������� �������� � ����������

	UCSR0C |= /*( 1 << URSEL0 ) |*/ // ��� ������� � �������� UCSRC ���������� ��� URSEL
		( 1 << UCSZ01 ) | ( 1 << UCSZ00 ); // ������ ������� � ����� 8 ���
}

// ���������� �� ��������� ������ ������ �� USART
ISR( USART0_RX_vect )
{
	cli();

	static byte b, usedLayersCount;
	b = UDR0; // ��������� ������ �� USART
	if( frameType == None )
		frameType = ( FrameType )b;
	else
	{
		if( frameType == PWMType )
		{
			pwmType = ( PWM )b;
			frameType = None;
		}
		else if( frameType == UsedLayers )
		{
			usedLayers = b;
			frameType = None;
			usedLayersCount = ( usedLayers & 1 ) + ( ( usedLayers >> 1 ) & 1 ) + ( ( usedLayers >> 2 ) & 1 ) + ( ( usedLayers >> 3 ) & 1 )
				+ ( ( usedLayers >> 4 ) & 1 ) + ( ( usedLayers >> 5 ) & 1 ) + ( ( usedLayers >> 6 ) & 1 ) + ( ( usedLayers >> 7 ) & 1 );
			if( usedLayersCount <= 4 )
				timerStep = 32;
			else
				timerStep = 16;
		}
		else if( frameType == Data512 )
		{
			if( byteCounter > 255 )
				states512High[( byte )( byteCounter >> 8 )] = b;
			else
				states512Low[( byte )byteCounter] = b;

			if( byteCounter == 511 )
			{
				byteCounter = 0;
				frameType = None;
			}
			else
				byteCounter++;
		}
		else if( frameType == Data64 )
		{
			states64[( byte )byteCounter] = b;

			if( byteCounter == 63 )
			{
				s64Update = true;

				byteCounter = 0;
				frameType = None;
			}
			else
				byteCounter++;
		}
		else if( frameType == Data8 )
		{
			states8[( byte )byteCounter] = b;

			if( byteCounter == 7 )
			{
				byteCounter = 0;
				frameType = None;
			}
			else
				byteCounter++;
		}
	}

	sei();
}

void setup()
{
	DDRA = 255;			// data
	DDRB |= 3;			// B00000011 // B0 - clock, B1 - latch
	DDRC = 255;			// layers

	UARTInit();
	sei();

	// 	for( byte i = 0; i < 9; i++ )
	// 	states512Low[i] = 1;
	// 	states512Low[0] = 255;
}

int main( void )
{
	setup();

	byte timer = 0;
	byte data = 0;
	byte state = 0;

	while( true )
	{
		if( pwmType == OnColumns )
		{
			for( byte layersLow = 0; layersLow < 4; ++layersLow )
			{
				if( usedLayers & ( 1 << layersLow ) )
				{
					byte layerOffset = layersLow << 6;
					for( byte iByte = 0; iByte < 8; ++iByte )
					{
						for( byte iBit = 56; iBit != 248; iBit -= 8 )
						{
							data <<= 1;
							state = states512Low[iBit + iByte + layerOffset];
							data |= state >= timer && state;
						}
						regData[iByte] = data;
					}

					PORTC = 0;
					regOut();
					PORTC |= 1 << layersLow;
				}
			}

			for( byte layersHigh = 0; layersHigh < 4; ++layersHigh )
			{
				if( usedLayers & ( 1 << ( layersHigh + 4 ) ) )
				{
					byte layerOffset = layersHigh << 6;
					for( byte iByte = 0; iByte < 8; ++iByte )
					{
						for( byte iBit = 56; iBit != 248; iBit -= 8 )
						{
							data <<= 1;
							state = states512High[iBit + iByte + layerOffset];
							data |= state >= timer && state;
						}
						regData[iByte] = data;
					}

					PORTC = 0;
					regOut();
					PORTC |= 1 << ( layersHigh + 4 );
				}
			}

			timer += timerStep;
		}
		else if( pwmType == OnLayers )
		{
			if( s64Update )
			{
				cli();
				s64Update = false;
				sei();

				for( byte iByte = 0; iByte < 8; ++iByte )
				{
					for( byte iBit = 56; iBit != 248; iBit -= 8 )
					{
						data <<= 1;
						data |= states64[iBit + iByte] > 0;
					}
					regData[iByte] = data;
				}

				regOut();
			}

			for( byte iLayer = 7; iLayer != 255; --iLayer )
			{
				data <<= 1;
				data |= states8[iLayer] >= timer && states8[iLayer];
			}

			PORTC = data;
			timer++;
		}
	}
}