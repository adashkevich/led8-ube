#pragma once

#include <Arduino.h>


class LedCube
{
public:
	struct Register 
	{
		int subordinateColumn[8];
	};

private:
	struct Column
	{
		Column()
		{
			registerIndex = registerBit = 0;
		}
		int registerIndex;
		int registerBit;
	};

public:
	LedCube( int clockPin, int dataPin, int* latchesPins, int* layersPins, Register* registerColumnMap, int cubeSize );
	~LedCube();

	int*** getState();

	void display();

private:
	int clockPin;
	int dataPin;
	int* latchesPins;
	int* layersPins;
	int registerCount;
	Column* columns;
	int cubeSize;

	int*** state;
	uint8_t* regsData;
};

