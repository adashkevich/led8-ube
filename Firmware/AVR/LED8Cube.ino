#include "LedCube.h"

//int cubeSize = 8;
//
//int clockPin = 40;
//int dataPin = 38;
//int latchesPins[] = { 30, 32, 34, 36, 42, 44, 46, 48 };
//int layersPins[] = { 33, 35, 37, 39, 41, 43, 45, 47 };
//LedCube::Register registers[] = {	{ 0, 1, 2, 3, 4, 5, 6, 7 },
//									{ 8, 9, 10, 11, 12, 13, 14, 15 },
//									{ 16, 17, 18, 19, 20, 21, 22, 23 },
//									{ 24, 25, 26, 27, 28, 29, 30, 31 },
//									{ 32, 33, 34, 35, 36, 37, 38, 39 },
//									{ 40, 41, 42, 43, 44, 45, 46, 47 },
//									{ 48, 49, 50, 51, 52, 53, 54, 55 },
//									{ 56, 57, 58, 59, 60, 61, 62, 63 } };

int cubeSize = 3;

int clockPin = 40;
int dataPin = 38;
int latchesPins[] = { 30, 32 };
int layersPins[] = { 33, 35, 37 };
LedCube::Register registers[] = {	{ 0, 1, 2, 3, 4, 5, 6, 7 },
									{ 8, -1, -1, -1, -1, -1, -1, -1 } };

LedCube ledCube( clockPin, dataPin, latchesPins, layersPins, registers, cubeSize );

void setup()
{

}

void loop()
{
	for( int z = 0; z < cubeSize; z++ )
	{
		ledCube.getState()[0][0][z] = 1;
		ledCube.getState()[2][2][z] = 1;
	}
	ledCube.display();

	delay( 500 );
}