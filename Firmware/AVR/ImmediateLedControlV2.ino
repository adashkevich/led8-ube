typedef unsigned char byte;

byte statesLow[256];
byte statesHigh[256];

void setup()
{
	DDRA = 255;			// data
	DDRB |= B00000011;  // B0 - clock, B1 - latch
	DDRC = 255;			// layers

	/*for( byte i = 0; i < 3; i++ )
		statesLow[i] = 1;
	statesLow[9] = 255;*/
}

void loop()
{
	byte timer = 0;
	byte data = 0;

	while( true )
	{
		for( byte layersLow = 0; layersLow < 4; ++layersLow )
		{
			PORTC = 0;

			byte layerOffset = layersLow << 6;
			for( byte iByte = 7; iByte != 255; --iByte )
			{
				for( byte iBit = 56; iBit != 248; iBit -= 8 )
				{
					data <<= 1;
					data |= statesLow[iBit + iByte + layerOffset] >= timer && statesLow[iBit + iByte + layerOffset];
				}

				PORTA = data;
				PORTB &= ~( 1 << B0 );
				PORTB |= 1 << B0;
			}
			PORTB &= ~( 1 << B1 );
			PORTB |= 1 << B1;

			PORTC |= 1 << layersLow;
		}

		for( byte layersHigh = 0; layersHigh < 4; ++layersHigh )
		{
			PORTC = 0;

			byte layerOffset = layersHigh << 6;
			for( byte iByte = 7; iByte != 255; --iByte )
			{
				for( byte iBit = 56; iBit != 248; iBit -= 8 )
				{
					data <<= 1;
					data |= statesHigh[iBit + iByte + layerOffset] >= timer && statesHigh[iBit + iByte + layerOffset];
				}

				PORTA = data;
				PORTB &= ~( 1 << B0 );
				PORTB |= 1 << B0;
			}
			PORTB &= ~( 1 << B1 );
			PORTB |= 1 << B1;

			PORTC |= 1 << ( layersHigh + 4 );
		}

		//delay( 1000 );

		timer++;
	}
}