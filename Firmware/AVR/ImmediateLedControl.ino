// ������������ ������--------------------------------------------------------------------------------------------
volatile uint8_t& clockPort = PORTG;
uint8_t clockPin = 1; // 40

volatile uint8_t& dataPort = PORTD;
uint8_t dataPin = 7; // 38

volatile uint8_t* latchesPorts[] = { &PORTC, &PORTC, &PORTC, &PORTC, &PORTL, &PORTL, &PORTL, &PORTL };
uint8_t latchesPins[] = { 7, 5, 3, 1, 7, 5, 3, 1 }; // 30, 32, 34, 36, 42, 44, 46, 48

volatile uint8_t* layersPorts[] = { &PORTC, &PORTC, &PORTC, &PORTG, &PORTG, &PORTL, &PORTL, &PORTL };
uint8_t layersPins[] = { 4, 2, 0, 2, 0, 6, 4, 2  }; // 33, 35, 37, 39, 41, 43, 45, 47

uint8_t states[512] = {};
int dataPointer = 0;

inline void setDDR()
{
	DDRG |= 1 << clockPin;
	DDRD |= 1 << dataPin;

	DDRC |= 1 << latchesPins[0];
	DDRC |= 1 << latchesPins[1];
	DDRC |= 1 << latchesPins[2];
	DDRC |= 1 << latchesPins[3];
	DDRL |= 1 << latchesPins[4];
	DDRL |= 1 << latchesPins[5];
	DDRL |= 1 << latchesPins[6];
	DDRL |= 1 << latchesPins[7];

	DDRC |= 1 << layersPins[0];
	DDRC |= 1 << layersPins[1];
	DDRC |= 1 << layersPins[2];
	DDRG |= 1 << layersPins[3];
	DDRG |= 1 << layersPins[4];
	DDRL |= 1 << layersPins[5];
	DDRL |= 1 << layersPins[6];
	DDRL |= 1 << layersPins[7];
}
//----------------------------------------------------------------------------------------------------------------

inline void fastShiftOut( volatile uint8_t& clockPort, uint8_t clockPin, volatile uint8_t& dataPort, uint8_t dataPin, uint8_t data ) // �� ������ ������ ����� ����� ������������� �������
{
	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 7 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 6 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 5 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 4 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 3 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 2 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( ( data >> 1 ) & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( data & 1 ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;
}

inline void pwmShiftOut( unsigned char timer, int offset )
{
	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 7] >= timer && states[offset + 7] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 6] >= timer && states[offset + 6] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 5] >= timer && states[offset + 5] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 4] >= timer && states[offset + 4] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 3] >= timer && states[offset + 3] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 2] >= timer && states[offset + 2] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset + 1] >= timer && states[offset + 1] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;

	dataPort = ( dataPort & ~( 1 << dataPin ) ) | ( ( states[offset] >= timer && states[offset] ) << dataPin );
	clockPort &= ~( 1 << clockPin );
	clockPort |= 1 << clockPin;
}

void setup()
{
	setDDR();

	clockPort &= ~( 1 << clockPin );
	dataPort &= ~( 1 << dataPin );

	for( int i = 0; i < 8; i++ )
	{
		*( latchesPorts[i] ) &= ~( 1 << latchesPins[i] );
		*( layersPorts[i] ) &= ~( 1 << layersPins[i] );
	}

	/*for( int i = 0; i < 512; i += 65 )
		states[i] = 3;*/
	/*for( int i = 0; i < 8; i++ )
		states[i] = 255;*/
}

void loop()
{
	unsigned char timer = 0;
	uint8_t data = 0;

	while( true )
	{
		for( int layer = 0; layer < 8; layer++ )
		{
			if( layer == 0 )
				*( layersPorts[7] ) &= ( 1 << layersPins[7] );
			else
				*( layersPorts[layer - 1] ) &= ( 1 << layersPins[layer - 1] );
			for( int reg = 0; reg < 8; reg++ )
			{
				*( latchesPorts[reg] ) &= ~( 1 << latchesPins[reg] );
				pwmShiftOut( timer, layer * 64 + reg * 8 );
				*( latchesPorts[reg] ) |= 1 << latchesPins[reg];
				//delay( 1000 );
			}
			*( layersPorts[layer] ) |= 1 << layersPins[layer];

			//delay( 5 );
		}
		//delay( 1000 );
		timer++;
	}
}