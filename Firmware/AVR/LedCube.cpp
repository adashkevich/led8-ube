#include "LedCube.h"

LedCube::LedCube( int clockPin, int dataPin, int* latchesPins, int* layersPins, Register* registerColumnMap, int cubeSize )
{
	this->clockPin = clockPin;
	this->dataPin = dataPin;
	registerCount = ( int )ceil( ( double )cubeSize * cubeSize / 8.0 );
	this->latchesPins = new int[registerCount];
	this->layersPins = new int[registerCount];
	columns = new Column[cubeSize * cubeSize];
	this->cubeSize = cubeSize;

	for (int i = 0; i < cubeSize; ++i)
	{
		pinMode( layersPins[i], OUTPUT );
		this->layersPins[i] = layersPins[i];
	}
	
	for( int i = 0; i < registerCount; i++ )
	{
		this->latchesPins[i] = latchesPins[i];
		pinMode( latchesPins[i], OUTPUT );		
		
		for( int i2 = 0; i2 < 8; i2++ )
		{
			if( registerColumnMap[i].subordinateColumn[i2] != -1 )
			{
				columns[registerColumnMap[i].subordinateColumn[i2]].registerIndex = i;
				columns[registerColumnMap[i].subordinateColumn[i2]].registerBit = i2;
			}
		}
	}
	pinMode( clockPin, OUTPUT );
	pinMode( dataPin, OUTPUT );

	state = new int**[cubeSize];
	for( int x = 0; x < cubeSize; x++ )
	{
		state[x] = new int*[cubeSize];
		for( int y = 0; y < cubeSize; y++ )
		{
			state[x][y] = new int[cubeSize];
			for( int z = 0; z < cubeSize; z++ )
				state[x][y][z] = 0;
		}
	}
	regsData = new uint8_t[registerCount];
}

LedCube::~LedCube()
{
	delete latchesPins;
	delete layersPins;
	delete[] columns;

	for( int x = 0; x < cubeSize; x++ )
	{
		for( int y = 0; y < cubeSize; y++ )
			delete state[x][y];
		delete state[x];
	}
	delete state;
	delete regsData;
}

int*** LedCube::getState()
{
	return state;
}

void LedCube::display()
{
	for( int layer = 0; layer < cubeSize; layer++ )
	{
		digitalWrite( layersPins[layer], HIGH );

		for( int i = 0; i < registerCount; i++ )
			regsData[i] = 0;
		for( int i = 0; i < cubeSize * cubeSize; i++ )
		{
			if( state[i % cubeSize][i / cubeSize][layer] )
				regsData[columns[i].registerIndex] |= 1 << columns[i].registerBit;
		}

		for( int reg = 0; reg < registerCount; reg++ )
		{
			digitalWrite( latchesPins[reg], LOW );
			shiftOut( dataPin, clockPin, MSBFIRST, regsData[reg] );
			digitalWrite( latchesPins[reg], HIGH );
		}

		digitalWrite( layersPins[layer], LOW );
	}
}