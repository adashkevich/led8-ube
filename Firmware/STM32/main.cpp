#include <inttypes.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>

typedef unsigned char byte;

void out( byte* regData, uint32_t layer )
{
	for( uint32_t i = 0; i < 8; ++i )
	{
		GPIOB->BRR = ( ( uint16_t )255 << 8 ) & GPIO_Pin_0; // data and clock pins
		GPIOB->BSRR = regData[i] << 8;
		GPIOB->BSRR = GPIO_Pin_0;
	}

	static uint16_t map[] = { GPIO_Pin_1, GPIO_Pin_2, GPIO_Pin_3, GPIO_Pin_4, GPIO_Pin_8, GPIO_Pin_9, GPIO_Pin_10, GPIO_Pin_11 }; // layers pins
	GPIOA->BRR = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
	GPIOB->BRR = GPIO_Pin_1; // latch pin
	GPIOB->BSRR = GPIO_Pin_1;
	GPIOB->BSRR = map[layer];
}

int main()
{
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE );

	GPIO_InitTypeDef GPIOInitDesc;
	GPIOInitDesc.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15; // clock, latch, data0..7
	GPIOInitDesc.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIOInitDesc.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOB, &GPIOInitDesc );

	GPIOInitDesc.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11; // layers
	GPIO_Init( GPIOA, &GPIOInitDesc );

	byte states[512] = {};
	byte regData[8] = {};
	byte timer = 0;
	byte data = 0;
	byte state = 0;

	while( true )
	{
		for( uint32_t layerOffset = 0; layerOffset < 512; layerOffset += 64 )
		{
			for( uint32_t iElement = 0; iElement < 8; ++iElement )
			{
				for( uint32_t rowOffset = 0; rowOffset < 64; rowOffset += 8 )
				{
					data >>= 1;
					state = states[iElement + rowOffset + layerOffset];
					data |= ( state >= timer && state ) << 7;
				}
				regData[iElement] = data;
			}
			out( regData, layerOffset >> 6 );
		}

		timer += 1;
	}

	return 0;
}